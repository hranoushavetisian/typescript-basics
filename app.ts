// Language Features

// Times
function times (arr: number[], num?: number): number[] {
    let count:number = 1;
    if (num) {
        while(count < num) {
            arr.push(...arr);
            count ++;
        }
    }
    return arr
}

// console.log(times([1,2,3], 3)); // [1,2,3,1,2,3,1,2,3]
// console.log(times([1,2,3])); // [1,2,3,1,2,3]


// Array NTH Element
// Implement the everyNth function to get every nth element in the given array. By default, nth = 1.

function everyNth(arr: number[], num?: number): number[] {
    let newArr: number[] = [];
    if (num) {
        for (let i = 0; i < arr.length; ++i) {
            let div:number = i + 1;
            if (div % num === 0) {
                newArr.push(arr[i]);
            }
        }
        return newArr;
    }
    return arr
}

// console.log(everyNth([1, 2, 3, 4, 5, 6])); // [1,2,3,4,5,6]
// console.log(everyNth([1, 2, 3, 4, 5, 6], 3)); // [3,6]


// Logger
// Implement the logger function which will prepare data for logging.
// It should have default service configuration {serviceName: 'global', serviceId: 1}
function addToObject(obj: { prop?: string; }, prop: string, value: string) {
    obj.prop = value;
}

function logger(arr: string[], obj = {serviceName: 'global', serviceId: 1}) {
    let data = {};
    for (let i = 0; i < arr.length; ++i) {
        addToObject(data, `${obj.serviceId}-${i}`, `[${obj.serviceName}] ${arr[i]}`);
        // data[`${obj.serviceId}-${i}`] = `[${obj.serviceName}] ${arr[i]}`
    }
    return data
}

// {"3-0":"[auth_service] Wrong email","3-1":"[auth_service] Wrong password","3-2":"[auth_service] Success login"}
logger(['Wrong email', 'Wrong password', 'Success login'], {serviceName: 'auth_service', serviceId: 3});

// {"1-0":"[global] Fatal error","1-1":"[global] Data corrupted"}
logger(['Fatal error', 'Data corrupted']);



// Types

// Days to New Year
// Write a function to calculate the days left until 31.12.2021.

// It must accept the date in 2 formats: Date() object and a string in 'DD.MM.YYYY';

function getDaysToNewYear(date: Date | string) :number {
    let daysLeft: number;
    let newYear: Date = new Date(2022, 1, 1);
    if (typeof date === 'string') {
        let dateArr = date.split('.');
        date = new Date(+dateArr[2], +dateArr[1], +dateArr[0]);
    } 
    daysLeft = newYear.getTime() - date.getTime();
    daysLeft = daysLeft / (1000 * 60 * 60 *24);
    return daysLeft;
}

// console.log(getDaysToNewYear(new Date(2021, 12, 31)));
// console.log(getDaysToNewYear(new Date(2021, 5, 15)));
// console.log(getDaysToNewYear('22.04.2020'));



// Last to first
// Write a function to change the position of the first and the last characters in the given string.
function lastToFirst(str: string): string {
    let arr: string[] = str.split('');
    let temp: string = arr[0];
    arr[0] = arr[arr.length - 1];
    arr[arr.length - 1] = temp;
    str = arr.join('');
    return str
}

// console.log(lastToFirst('loop')); // pool;
// console.log(lastToFirst('ab')); // ba;
// console.log(lastToFirst('a')); // a;


// Group organization users
// You have an organization with 2 types of users EMPLOYEE and CONTRACTOR. 
// Create function to group an array of users to {employees: [], contractors: []} 
interface EMPLOYEE  {
    firstname: string,
    lastname: string,
    position: 'Employee',
};
interface CONTRACTOR  {
    firstname: string,
    lastname: string,
    position: 'Contractor',
}
function groupUsers(arr: (EMPLOYEE|CONTRACTOR)[]) {
    let users: any = {
        employees : [],
        constractors: []
    }
    for (let user of arr) {
        if (user.position === 'Employee') {
            users.employees.push(user);
        } else if (user.position === 'Contractor') {
            users.constractors.push(user);
        }
    }
    return users
}

let obiwan: EMPLOYEE = {
    firstname: 'Obi-Wan',
    lastname: 'Kenobi',
    position: 'Employee'
};

let ahsoka: EMPLOYEE = {
    firstname: 'Ahsoka',
    lastname: 'Tano',
    position: 'Employee',
};

let han: CONTRACTOR = {
    firstname: 'Han',
    lastname: 'Solo',
    position: 'Contractor'

};

let userArr = [];
userArr.push(obiwan, ahsoka, han);
// console.log(groupUsers(userArr));


// Classes

// Implement the functionality to create vehicles. All vehicles must implement the BaseVehicle interface.

// Each type of a vehicle has fuel consumption and fuel level
// When an engine starts, fuel consumption starts, and fuel consumption continues every second
// Refueling restores the maximum fuel level
// Each vehicle must have a model's name and a vendor's name.
// You must implement Cars. It should remain possible to implement new types of vehicles.

interface BaseVehicle {
    model: string,
    vendor: string,
    fuelLevel: number,
    fuelConsumption() :number,
    refuel() :number
    
}

class Car implements BaseVehicle {
    vendor: string;
    model: string;
    tankCapacity: number;
    fuelLevel: number;
    engineWork: boolean;

    constructor ( model: string, vendor: string, tankCapacity: number) {
        this.vendor = vendor;
        this.model = model;
        this.tankCapacity = tankCapacity;
        this.fuelLevel = tankCapacity;
        this.engineWork = false;
    }
    startEngine() :void {
        this.engineWork = true;
        this.fuelLevel -= 3;
        while(this.engineWork) {
            this.fuelConsumption();
        }
    }
    stopEngine() :void {
        this.engineWork = false;
        // return this.engineWork
    }
    fuelConsumption() :number {
        if (this.engineWork) {
            this.fuelLevel --;
        }
        return this.fuelLevel;
    }
    refuel() :number {
        this.fuelLevel = this.tankCapacity;
        return this.fuelLevel;
    }
}

// let cx5 = new Car ('mazda', 'cx5', 40);
// cx5.startEngine();
// console.log(cx5.fuelLevel);

// setTimeout(cx5.stopEngine, 10000);
// console.log(cx5.fuelLevel);


// Counter
// Implement a singleton class Counter with the functionality to store data. 
// It should implement 2 service methods getInstance, destroy and 3 methods with business logic - increase, decrease, getState.


let instance:null | Counter = null;
class Counter {
    num: number;

    constructor() {
        this.num = 0;
        if(!instance) {
            instance = this;
        } else { return this }
    }

    static getInstance() {
        return this
    }
    static destroy() {
        // delete this
    }

    increase(): void {
        this.num ++;
    }

    decrease() {
        this.num --;
    }

    getState() {
        return this.num;
    }

}


// const instance1 = Counter.getInstance();
// instance1.increase();
// console.log(instance1.getState()); // 1;
// const instance2 = Counter.getInstance();
// console.log(instance2.getState()); // 1;
// instance2.increase();
// console.log(instance1.getState()); // 2;
// console.log(instance2.getState()); // 2;
// Counter.destroy();
// Counter.getInstance().getState(); // 0